﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ConverterAnsari.Models
{
    public class Converter
    {   [ScaffoldColumn(false)]
        [Key]public int ZipCodeID { get; set; }

        [Required]
        [Range(1001, 99950, ErrorMessage = "Please enter a valid US ZIP Code")]
        [Display(Name = "ZIP Code")]
        public int ZipCode { get; set; }

        [Required]
        [Range(-12, 120,ErrorMessage = "The field Temperature (F) must be between -12 and 120")]
        [Display(Name = "Temperature (F)")]
        public int TemperatureF { get; set; }

        public Converter()
        {
            ZipCode = 64468;
            TemperatureF = 72;
        }

    }
}
