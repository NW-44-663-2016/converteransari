﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using ConverterAnsari.Models;

namespace ConverterAnsari.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            ViewData["Title"] = "Converter App by Ansari";
            ViewData["Result"] = "";
            Converter converter = new Converter();
            return View(converter); 
        }

        public IActionResult Convert(Converter converter)
        {
            if (ModelState.IsValid)
            {
                ViewData["Title"] = "Converted by Ansari";
                ViewData["Result"] = "Temperature in C = " + (int)((converter.TemperatureF - 32) * 5.0 / 9.0);
            }
            ViewData["Title"] = "Converted by Ansari";
            return View("Index", converter);
        }

        public IActionResult About()
        {
            return View();
        }
    }
}
